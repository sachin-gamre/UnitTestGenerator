# Project: UnitTestGenerator #

Objective: To auto generate Junit test skeleton classes for all java source files annotated with @GenerateUnitTest.

# Usage details #

Download the project, build it locally with below commands <br>

$ mvn clean install <br>

Above command will create a maven plugin and store it in maven repository. <br>
To deploy this plugin in your Nexus or JFrog artifactory, kindly change pom.xml to add artifactory details.<br>

# Pom.xml changes in target Project # 

1. In your project's pom.xml add below snippets

```HTML

<dependencies>
	<!-- Below dependency is needed to access @GenerateUnitTest annotation -->
	<dependency>
	    	<groupId>in.sach.gamre</groupId>
			<artifactId>UnitTestGenerator-maven-plugin</artifactId>
			<version>0.0.2-SNAPSHOT</version>
	</dependency>
</dependencies>

<!-- Add Pluging to generate Unit test cases -->
<!-- The plugin gets triggered at test-compile stage of maven packaging -->
<!-- Not applicable for POM packaging -->
<build>
	<plugins>
		<plugin>
  			<groupId>in.sach.gamre</groupId>
  			<artifactId>UnitTestGenerator-maven-plugin</artifactId>
  			<version>0.0.2-SNAPSHOT</version>
  			<executions>
  				<execution>
  					<id>execution1</id>
  					<configuration>
  						<basePackage>${projects base package , eg: in.sach.gamre.practice}</basePackage>
  					</configuration>
  					<goals>
  						<goal>GenerateUnitTest</goal>
  					</goals>
  				</execution>
  			</executions>
  		</plugin>
	</plugins>
</build>

```