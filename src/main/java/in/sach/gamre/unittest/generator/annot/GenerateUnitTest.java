package in.sach.gamre.unittest.generator.annot;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target( ElementType.TYPE )
public @interface GenerateUnitTest {
	
	boolean merge() default false;
	
	String[] testLibs();
	
}
