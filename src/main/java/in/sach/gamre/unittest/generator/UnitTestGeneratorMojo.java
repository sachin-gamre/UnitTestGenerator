package in.sach.gamre.unittest.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import in.sach.gamre.unittest.generator.annot.GenerateUnitTest;


/**
 * Auto generate JUnit test skeleton for java source classes annotated with @GenerateUnitTest annotation 
 * 
 * @author sgamre
 *
 */
@Mojo(name="GenerateUnitTest", defaultPhase=LifecyclePhase.TEST_COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE)
public class UnitTestGeneratorMojo extends AbstractMojo {
	
	private static String SOURCE_FOLDER = Paths.get("src", "main","java").toString();
	
	private static String TEST_FOLDER = Paths.get("src","test","java").toString();
	
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;
	
	@Parameter(property = "scope")
	String scope;
	
	@Parameter(property = "basePackage", required = true)
	String basePackage;
	
	@Parameter(property = "importTestLibs", required = true)
	private String[] importTestLibs;
	
	@Parameter(property = "updateExisting", defaultValue = "false")
	private boolean updateExisting;
	
	/**
	 * Extracts the class name from the given Java source files.
	 * 
	 * @param absolutePath
	 * @return
	 */
	private String getClass(String absolutePath) {
		getLog().info("Source file -> " + absolutePath);
		String retFile = absolutePath.replaceAll("\\\\|\\/", ".")
				.replaceAll(project.getBasedir().getAbsolutePath().replaceAll("\\\\|\\/", "."), "")
				.replaceAll(SOURCE_FOLDER.replaceAll("\\\\|\\/", "."), "").replaceAll("\\\\|\\/", ".");
		return retFile.replaceAll("^\\.+", "").replace(".java", "");
	}
	
	/**
	 * Generates Junit test file name.
	 * 
	 * @param absolutePath
	 * @param className
	 * @return
	 */
	private String generateTestClassFileName(String absolutePath, String className) {
		return absolutePath.replaceAll("\\\\|\\/", "%%")
				.replaceAll(SOURCE_FOLDER.replaceAll("\\\\|\\/", "%%"), TEST_FOLDER.replaceAll("\\\\|\\/", "%%"))
				.replaceAll("%%", Matcher.quoteReplacement(File.separator)).replaceAll(className, className.replaceAll(".java", "Test.java"));
	}
	
	/**
	 * Returns Test package name based on Java source package.
	 * 
	 * @param testClassFileName
	 * @param testClassName
	 * @return
	 */
	private String getTestPackage(String testClassFileName, String testClassName) {
		String[] fileParts = testClassFileName.replaceAll("\\\\|\\/", "%%").split(TEST_FOLDER.replaceAll("\\\\|\\/", "%%"));
		return fileParts[1].replaceAll("%%", ".").replaceAll(testClassName, "").replaceAll("^\\.|\\.$", "");
	}
	
	/**
	 * Returns Test class
	 * 
	 * @param testClassFileName
	 * @param testClassName
	 * @return
	 */
	private String getTestClass(String testClassFileName, String testClassName) {
		return testClassName.replaceAll(".java", "");
	}
	
	/**
	 * Generates test method name.
	 * 
	 * @param actualMethodName
	 * @return
	 */
	private String generateTestMethodName(String actualMethodName) {
		return "test" + actualMethodName.substring(0, 1).toUpperCase() + actualMethodName.substring(1);
	}
	
	
	public void getMethodBody(Method currentMethod, FileWriter writer, String classObjectVariableName) throws IOException {
		
		int paramCounter = 1;
		String paramName = null;
		List<String> paramList = new ArrayList<String>();
		if (currentMethod.getParameterCount() > 0) {
			java.lang.reflect.Parameter[] params = currentMethod.getParameters();
			for (java.lang.reflect.Parameter param: params) {
				paramName = "param".concat(""+paramCounter++);
				paramList.add(paramName);
				writer.write("\t\t" + param.getType().getTypeName().concat(" ").concat(paramName).concat("= null;\n"));
			}
			writer.write("\n");
		}
		
		String returnType = currentMethod.getReturnType().getCanonicalName();
		if (returnType == "void") {
			writer.write("\t\tassertAll(() -> "+ classObjectVariableName +"." + currentMethod.getName() + "(" + String.join(",", paramList) + "), null);\n");
		} else {
			writer.write("\t\t" + currentMethod.getReturnType().getCanonicalName() + " returnType = null;\n");
			writer.write("\t\tassertEquals("+ classObjectVariableName +"." + currentMethod.getName() + "(" + String.join(",", paramList) + "), returnType);\n\n");
		}
	}
	
	/**
	 * Main method, which add skeleton code to Junit test.
	 * 
	 * @param testClassFileName
	 * @param origClassObject
	 * @param testLibs
	 * @throws Exception
	 */
	private void generateTestSource(String testClassFileName, Class<?> origClassObject, String[] testLibs, boolean updateExisting) throws Exception {
		File f = new File(testClassFileName);
		if (!f.exists()) {
			// Create test class with directory
			File parentDirectory = new File(f.getParent());
			if (!parentDirectory.exists()) {
				if(!parentDirectory.mkdirs()) {
					throw new Exception("Unable to create directory");
				}
			}
			
			if(!f.createNewFile()) {
				return;
			}
			
			// Add source code to test class.
			FileWriter writer = new FileWriter(f);
			
			// Write Package
			writer.write("package " + getTestPackage(testClassFileName, f.getName()) + ";\n\n");
			
			// Write imports
			if (testLibs != null && testLibs.length > 0) {
				for (int i = 0; i < testLibs.length; i++) {
					writer.write("import " + testLibs[i] + ";\n\n");
				}
				// Import the class under test.
				writer.write("import " + origClassObject.getCanonicalName() + ";\n\n");
			}
			
			// Write class
			String classObjectVariable = "v".concat(origClassObject.getSimpleName());
			writer.write("public class " + getTestClass(testClassFileName, f.getName()) + "{\n\n");
			writer.write("\tprivate " + origClassObject.getSimpleName() + " " + classObjectVariable + ";\n\n");
						
			// Write methods
			Method[] methodList = origClassObject.getDeclaredMethods();
			if (methodList.length > 0) {
				for (int i = 0; i < methodList.length; i++) {
					Method currentMethod = methodList[i];
					writer.write("\t@Test\n");
					writer.write("\tpublic void " + generateTestMethodName(currentMethod.getName()) + "() {\n\n");
					getMethodBody(currentMethod, writer, classObjectVariable);
					writer.write("\t}\n\n");
				}
			}
			
			// Write end
			writer.write("}\n\n");
			writer.close();
		}
	}
	
	/**
	 * Parses the source project for all Java source code files.
	 * Checks if any given Java class is annotated with @GenerateUnitTest
	 * if yes, then it creates Junit skeleton for that class.
	 * 
	 * @param folder
	 * @throws MojoExecutionException
	 * @throws DependencyResolutionRequiredException
	 * @throws MalformedURLException
	 */
	private void parseFilesAndFolders(final File folder) throws MojoExecutionException, DependencyResolutionRequiredException, MalformedURLException {
		
		List runtimeClasspathElements = project.getCompileClasspathElements();
		URL[] runtimeUrls = new URL[runtimeClasspathElements.size()];
		for (int i = 0; i < runtimeClasspathElements.size(); i++) {
		  String element = (String) runtimeClasspathElements.get(i);
		  runtimeUrls[i] = new File(element).toURI().toURL();
		}
		URLClassLoader newLoader = new URLClassLoader(runtimeUrls,
		  Thread.currentThread().getContextClassLoader());

		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            parseFilesAndFolders(fileEntry);
	        } else {
	        	if (fileEntry.getAbsolutePath().contains(SOURCE_FOLDER) && fileEntry.getName().endsWith(".java")) {
	        		try {
	        			Class<?> c = newLoader.loadClass(getClass(fileEntry.getAbsolutePath()));
						String testClass = generateTestClassFileName(fileEntry.getAbsolutePath(), fileEntry.getName());
						GenerateUnitTest annot = c.getAnnotation(GenerateUnitTest.class);
						if (annot != null) {
							generateTestSource(testClass, c, importTestLibs, updateExisting);
						}
	        		} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
	        	}
	        }
	    }
	}
	
	public void execute() throws MojoExecutionException, MojoFailureException {
	    try {
			parseFilesAndFolders(project.getBasedir());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DependencyResolutionRequiredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}

}
